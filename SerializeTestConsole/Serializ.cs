﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization.Formatters.Soap;

namespace SerializeTestConsole
{
    static class XmlSerial
    {
        public static void SerialToFileXml(this Human[] hm, string path)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Human[]));
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, hm);
            }
        }

        public static Human[] DeserializeInFileXml(string path)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Human[]));
            Human[] hm;
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                hm = (Human[]) formatter.Deserialize(fs);
                
            }
            return hm;
        }
        
    }
    static class BinarySerial
    {
        public static void SerialToFileBin(this Human[] hm, string path)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs,hm);
            }
        }

        public static Human[] DeserializeInFileBat(string path)
        {
            Human[] hm;

            BinaryFormatter formatter = new BinaryFormatter();

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                 hm = (Human[])formatter.Deserialize(fs);
            }
            return hm;
        }

    }
    static class JsonSereal
    {
        public static void SerialToFileJson(this Human[] hm, string path )
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(Human[]));
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                jsonFormatter.WriteObject(fs, hm);
            }
        }

        public static Human[] DeserializeInFileJson(string path)
        {
            Human[] hm;

            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(Human[]));

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                hm = (Human[])jsonFormatter.ReadObject(fs);
            }
            return hm;
        }
    }
    static class SoapSereal
    {
        public static void SerialToFileSoap(this Human[] hm, string path)
        {
            SoapFormatter formatter = new SoapFormatter();
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, hm);
            }
        }

        public static Human[] DeserializeInFileSoap(string path)
        {
            Human[] hm;

            SoapFormatter formatter = new SoapFormatter();
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                hm = (Human[])formatter.Deserialize(fs);
            }
            return hm;
        }
    }
    
}
