﻿using System;

namespace SerializeTestConsole
{
    [Serializable]
    public class Human
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Human()
        {
        }
        public Human(int ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }
    }
}
