﻿using System;
using System.Diagnostics;
using System.IO;


namespace SerializeTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            Console.WriteLine("Amount of elements: ");
            int n = Int32.Parse(Console.ReadLine());

            sw.Start();
            Human[] hm = MassGenerate(n);
            sw.Stop();
            Console.WriteLine("Create: " + sw.ElapsedMilliseconds + " ms");

            #region Xml
            Console.WriteLine("---------XML---------");
            sw.Reset(); sw.Start();
            hm.SerialToFileXml("TestXML.xml");
            sw.Stop();
            Console.WriteLine("Serialize: " + sw.ElapsedMilliseconds + " ms");

            sw.Reset(); sw.Start();
            Human[] nm2 = XmlSerial.DeserializeInFileXml("TestXML.xml");
            sw.Stop();
            Console.WriteLine("Deserialize: " + sw.ElapsedMilliseconds + " ms");
            FileInf("TestXML.xml");
            #endregion

            #region Binary
            Console.WriteLine("---------Binary---------");
            sw.Reset(); sw.Start();
            hm.SerialToFileBin("testBin.dat");
            sw.Stop();
            Console.WriteLine("Serialize: " + sw.ElapsedMilliseconds + " ms");
            sw.Reset(); sw.Start();
            nm2 = BinarySerial.DeserializeInFileBat("testBin.dat");
            sw.Stop();
            Console.WriteLine("Deserialize: " + sw.ElapsedMilliseconds + " ms");
            FileInf("testBin.dat");
            #endregion

            #region Json
            Console.WriteLine("---------Json---------");
            sw.Reset(); sw.Start();
            hm.SerialToFileJson("testJson.json");
            sw.Stop();
            Console.WriteLine("Serialize: " + sw.ElapsedMilliseconds + " ms");
            sw.Reset(); sw.Start();
            nm2 = JsonSereal.DeserializeInFileJson("testJson.json");
            sw.Stop();
            Console.WriteLine("Deserialize: " + sw.ElapsedMilliseconds + " ms");
            FileInf("testJson.json");
            #endregion

            #region Soap
            Console.WriteLine("---------Soap---------");
            sw.Reset(); sw.Start();
            hm.SerialToFileSoap("testSoap.soap");
            sw.Stop();
            Console.WriteLine("Serialize: " + sw.ElapsedMilliseconds + " ms");
            sw.Reset(); sw.Start();
            nm2 = SoapSereal.DeserializeInFileSoap("testSoap.soap");
            sw.Stop();
            Console.WriteLine("Deserialize: " + sw.ElapsedMilliseconds + " ms");
            FileInf("testSoap.soap");
            #endregion

            Console.ReadKey();
        }

        static Human[] MassGenerate(int n)
        {
            Random r = new Random();
            Human[] hm = new Human[n];
            for (int i = 0; i < n; i++)
            {
                hm[i] = new Human(r.Next(), "Kiril" + r.Next());
            }
            return hm;
        }
        static void FileInf(string path)
        {
            FileInfo fi = new FileInfo(path);
            Console.WriteLine(path+" - " + fi.Length + " b");
        }
    }
}
